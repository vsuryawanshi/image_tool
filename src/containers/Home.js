import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import Snackbar from 'material-ui/Snackbar';
import axios from "axios";

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            globalToken : "demo@mina",
            imagesArray:[],
            tagsArray:[],
            selectedTags:[],
            imageDialogOpen:false,
            imageUrl:"",
            imageAddError:"",
            tagDialogOpen:false,
            tagName:"",
            tagValue:"",
            tagNameError:"",
            tagValueError:"",
            snackBarOpen:false,
            snackbarText:""
        }
    }

    componentDidMount() {
        this.getCurrentImages();   
    }

    showSnackBar(message){
        this.setState({snackbarText:message, snackBarOpen:true})
    }

    getCurrentImages(){
        axios.get("http://51.15.185.175:8082/get/image?token="+this.state.globalToken).then((response)=>{
            this.setState({imagesArray:response.data.data.image, tagsArray:response.data.data.tags})
        }).catch((error)=>{
            console.error(error);
        })
    }

    checkUrl(url){
        var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
        var regex = new RegExp(expression);
        return url.match(regex);
    }

    addImage(){
        if(this.checkUrl(this.state.imageUrl)){
            this.setState({
                imageAddError:""
            },()=>{
                this.showSnackBar("Image added successfully");
                axios.get("http://51.15.185.175:8082/add/image?image=" + this.state.imageUrl +"&token=" + this.state.globalToken).then((response)=>{
                    this.setState({
                        imageDialogOpen:false,
                        imageUrl:""
                    },()=>{
                        this.getCurrentImages();
                    })
                }).catch((error)=>{
                    this.showSnackBar("Could not add image, please try again");
                })
            })
        }else{
            this.setState({
                imageAddError:"Enter value is not an URL, please check"
            });
        }
    }

    addTag(){
        if(this.state.tagName == ""){
            this.setState({tagNameError:"Name cannot be empty"});
        }
        if(this.state.tagValue == ""){
            this.setState({tagValueError:"Value cannot be empty"});
        }
        if(this.state.tagName !== "" && this.state.tagValue !== ""){
            axios.get("http://51.15.185.175:8082/add/tag?tname="+ this.state.tagName +"&tval=" + this.state.tagValue + "&token=" + this.state.globalToken).then((response)=>{
                if(response.data.status == 400){
                    //tag already exists
                    window.alert("Tag alreay exists, please select a different name");
                } else {
                    this.showSnackBar("Tag added successfully");
                    this.setState({
                        tagDialogOpen:false,
                        tagName:"",
                        tagValue:""
                    },()=>{
                        this.getCurrentImages();
                    });
                }
            }).catch((error)=>{
                this.showSnackBar("Could not add tag, please try again");
            })
        }
    }

    applySelectedTagNames(image,tags){
        axios.post("http://51.15.185.175:8082/tag/image",{
            "id":image.id,
            "url":image.image_url,
            "tags":tags.join(","),
            "token":this.state.globalToken
        }).then((response)=>{
            this.showSnackBar("Tags applied to image successfully");
            this.setState({
                selectedTags:[]
            },()=>{
                this.getCurrentImages();
            })
        })
    }

    render() {
        return (
            <div className="homepage-wrapper">
                <header className="head-wrap">
                    <div className="logo">Image Tool</div>
                    <div className="menu-wrap">
                            <div className="menu-item" >
                                Your Token:
                            </div>
                            <div className="menu-item" >
                                <TextField type="password" hintText="Enter Token" value={this.state.globalToken} onChange={(event)=>{this.setState({globalToken:event.target.value})}}/>   
                            </div>
                        </div>
                </header> 
                <section className="input-options">
                    <RaisedButton label="Add an image" primary={true} style={{"margin":"10px 20px"}} onClick={(event)=>{this.setState({imageDialogOpen:true})}}/>
                    <RaisedButton label="Add a Tag" primary={true} style={{"margin":"10px 20px"}} onClick={(event)=>{this.setState({tagDialogOpen:true})}}/>
                </section>  
                <section className="main-section">
                    {
                        this.state.imagesArray.length > 0 ?
                        <div className="images-wrap">
                            {
                                this.state.imagesArray.map((_image,index)=>{
                                    return(
                                        <div className="image-wrap" key={index}>
                                            <div className="img-holder" style={{"backgroundImage":"url( " + _image.image_url + ")"}}/>
                                            <h3 style={{"textAlign":"center","margin":"10px 0px"}}>Select Tags for this image</h3>
                                            <div className="chips-holder">
                                                {
                                                    this.state.tagsArray.map((tag)=>{
                                                        return(
                                                            <Chip
                                                                key={tag.id}
                                                                className="chipc"
                                                                onClick={(event)=>{
                                                                    if(this.state.selectedTags.indexOf(tag.id) == -1){
                                                                        this.setState(prevState => ({
                                                                            selectedTags : [...prevState.selectedTags, tag.id]
                                                                        }))
                                                                    } else {
                                                                        var array = this.state.selectedTags;
                                                                        var index = array.indexOf(tag.id)
                                                                        array.splice(index, 1);
                                                                        this.setState({selectedTags: array });
                                                                    }
                                                                }}>
                                                                {
                                                                    this.state.selectedTags.indexOf(tag.id) != -1   ?
                                                                    <Avatar size={32}>&#x2713;</Avatar>
                                                                    :
                                                                    null
                                                                }
                                                                {tag.tname}
                                                            </Chip>
                                                        );
                                                    })
                                                }
                                            </div>  
                                            <div className="btn-wrap">
                                                <RaisedButton label="Apply Tags" primary={true} style={{"margin":"10px auto"}} onClick={(event)=>{
                                                    var selectedTagNames = [];
                                                    this.state.tagsArray.map((tag)=>{
                                                        if(this.state.selectedTags.indexOf(tag.id) != -1){
                                                            selectedTagNames.push(tag.tname);
                                                        }
                                                    });
                                                    if(selectedTagNames.length > 0){
                                                        this.applySelectedTagNames(_image,selectedTagNames);
                                                    }
                                                }}/>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                        :
                        null
                    }
                </section> 
                <Dialog
                    title="Add an Image"
                    actions={[
                        <FlatButton
                            label="Cancel"
                            primary={true}
                            onClick={(event)=>{
                                this.setState({imageDialogOpen : false})
                            }}
                        />,
                        <FlatButton
                            label="Submit"
                            primary={true}
                            onClick={this.addImage.bind(this)}
                        />,
                        ]}
                    open={this.state.imageDialogOpen}>
                        <div className="dialog-wrap">
                            <div className="lbl">Enter image url</div>
                            <TextField 
                                id="imgurl"
                                fullWidth={true} 
                                hintText="Image URL" 
                                value={this.state.imageUrl} 
                                errorText={this.state.imageAddError}
                                onChange={(event)=>{
                                    this.setState({imageUrl:event.target.value})
                                }}/>  
                            <div className="dialog-image-preview-wrap">
                                <div className="dialog-image-preview" style={{"backgroundImage":"url( " + this.state.imageUrl + ")"}}/>
                            </div> 
                            <div style={{"textAlign":"center"}}>Image Preview</div>
                        </div>
                    </Dialog>

                <Dialog
                    title="Add a Tag"
                    actions={[
                        <FlatButton
                            label="Cancel"
                            primary={true}
                            onClick={(event)=>{
                                this.setState({tagDialogOpen : false})
                            }}
                        />,
                        <FlatButton
                            label="Submit"
                            primary={true}
                            onClick={this.addTag.bind(this)}
                        />,
                        ]}
                    open={this.state.tagDialogOpen}>
                        <div className="tag-dialog-wrap">
                            <div className="tag-section">
                                <div className="lbl">Tag Name:</div>
                                <TextField 
                                    id="tname"
                                    fullWidth={true}
                                    value={this.state.tagName} 
                                    placeholder="ex. TagOne"
                                    errorText={this.state.tagNameError}
                                    onChange={(event)=>{
                                        this.setState({tagName:event.target.value})
                                    }}/>   
                            </div>
                            <div className="tag-section">
                                <div className="lbl">Tag Value (Comma Separated):</div>
                                <TextField 
                                    id="tval"
                                    fullWidth={true}  
                                    value={this.state.tagValue} 
                                    errorText={this.state.tagValueError}
                                    placeholder="ex. Abc, Xyz"
                                    onChange={(event)=>{
                                        this.setState({tagValue:event.target.value})
                                    }}/>   
                            </div>
                        </div>
                </Dialog>
                <Snackbar
                    open={this.state.snackBarOpen}
                    message={this.state.snackbarText}
                    autoHideDuration={4000}
                    onRequestClose={(event)=>{
                        this.setState({snackBarOpen:false})
                    }}/>
            </div>
        );
    }
}