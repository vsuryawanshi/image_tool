import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

//Main Application Wrapper
export default class App extends Component {
    render(){
        return (
            <MuiThemeProvider>
                <div className="page-wrap">
                    {this.props.children}
                </div>    
            </MuiThemeProvider>
        );
    }
}