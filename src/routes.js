import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import App from './containers/App';
import Home from './containers/Home';
import NotFoundPage from './containers/NotFoundPage.js';

export default (
    <Route>
        <Route path="/" component={App}>
            <IndexRedirect to="/home" />
            <Route path="home" component={Home} />
            <Route path="*" component={NotFoundPage}/>
        </Route>
        
  </Route>
);
