import React, {Component} from "react";

export default class Accordion extends Component{
    constructor(props){
        super(props);
        this.state={
            isExpanded:false
        };
    }

    render(){
        return(
            <div className="accordion__wrap">
                <div className="accordion__header">
                    <div className="accordion__title">{this.props.label}</div>
                </div>
                <img className="expand__icon" src={require("../images/clear-button.svg")}/>
            </div>
        )
    }
}